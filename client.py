# This is the test implementation of the client for the SYSC 4907 Vision Based Control Robotics project
import os
import sys
import threading
import cv2
import communication, settings
from PyQt5 import QtWidgets, QtGui, QtCore


# The client window class defines the main window for the client
class ClientWindow(QtWidgets.QWidget):

    # Hold the server IP address and port number
    serverIP = "192.168.0.31"
    serverPort = 9999

    # Constructor
    def __init__(self):
        super(ClientWindow, self).__init__()
        # Set up the geometry for the window
        self.setWindowScale(1000, 680)
        # Set up the window title
        self.setWindowTitle("Vision Based Control Robotics Client Test App")
        # Default to not connected
        self.bConnected = False
        # Flag for direction command sent
        self.bCmdSent = False

        # Set up the settings window
        self.settingsWindow = settings.ClientSettingsWindow()
        # Update the server IP address and port number based on settings
        self.serverIP = self.settingsWindow.ServerIPAddr
        self.serverPort = self.settingsWindow.ServerPortNum

        # Set up the UI
        self.setupUI()

        # Set up the communication layer
        self.workerClient = communication.ClientWorker(self.settingsWindow.BufferSize)
        self.videoDisplay.sigResizeVideo.connect(self.workerClient.scaleVideoSlot)
        self.workerClient.sigVideoFeed.connect(self.videoDisplay.setImageSlot)
        self.workerClient.start()  # Start the client worker

        # Set up the connect button event
        self.btnConnect.clicked.connect(self.handleConnectionSlot)
        # Set up the settings button event
        self.btnSettings.clicked.connect(self.settingsBtnSlot)

        # Update the UI
        self.updateUI()

    # Helper function to set up the UI
    def setupUI(self):
        # Video Display
        self.videoDisplay = communication.VideoDisplay()

        # Server IP Address
        lblIPHint = QtWidgets.QLabel("Server IP Address:")
        lblIPHint.setStyleSheet("color: blue;")
        self.leIPInput = QtWidgets.QLineEdit(self.serverIP)
        # Server Port Number
        lblPortHint = QtWidgets.QLabel("Port Number:")
        lblPortHint.setStyleSheet("color: blue;")
        self.lePortInput = QtWidgets.QLineEdit(str(self.serverPort))
        # Send command
        lblSendCmdHint = QtWidgets.QLabel("Send Command:")
        lblSendCmdHint.setStyleSheet("color: blue;")
        self.lblSendCmd = QtWidgets.QLabel()
        self.lblSendCmd.setStyleSheet("font-size: 10pt; border: 1px solid gray;")
        # Settings button
        lblSettingsHint = QtWidgets.QLabel("Settings:")
        lblSettingsHint.setStyleSheet("color: blue;")
        self.btnSettings = QtWidgets.QPushButton()
        self.btnSettings.setIcon(QtGui.QIcon("icon/settings.png"))
        # Connect button
        lblConnectHint = QtWidgets.QLabel("Server Connection:")
        lblConnectHint.setStyleSheet("color: blue;")
        self.btnConnect = QtWidgets.QPushButton("Connect")

        # Set up the layout
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setContentsMargins(0, 0, 0, 8)
        self.gridLayout.addWidget(self.videoDisplay, 0, 0, 1, 7)
        self.gridLayout.addWidget(lblIPHint, 1, 1)
        self.gridLayout.addWidget(lblPortHint, 1, 2)
        self.gridLayout.addWidget(lblSendCmdHint, 1, 3)
        self.gridLayout.addWidget(lblSettingsHint, 1, 4)
        self.gridLayout.addWidget(lblConnectHint, 1, 5)
        self.gridLayout.addWidget(self.leIPInput, 2, 1)
        self.gridLayout.addWidget(self.lePortInput, 2, 2)
        self.gridLayout.addWidget(self.lblSendCmd, 2, 3)
        self.gridLayout.addWidget(self.btnSettings, 2, 4)
        self.gridLayout.addWidget(self.btnConnect, 2, 5)
        # Row and column stretch
        self.gridLayout.setRowStretch(0, 1)
        self.gridLayout.setColumnStretch(1, 10)
        self.gridLayout.setColumnStretch(2, 10)
        self.gridLayout.setColumnStretch(3, 10)
        self.gridLayout.setColumnStretch(4, 5)
        self.gridLayout.setColumnStretch(5, 5)
        # Set the layout
        self.setLayout(self.gridLayout)

    # Update the UI display
    def updateUI(self):
        # Check for connection status
        if self.bConnected:
            # Update the text for the connection button
            self.btnConnect.setText("Disconnect")
            # Disable the inputs for server IP and port number
            self.leIPInput.setEnabled(False)
            self.lePortInput.setEnabled(False)
        else:
            # Update the text for the connection button
            self.btnConnect.setText("Connect")
            # Enable the inputs for the server IP and port number
            self.leIPInput.setEnabled(True)
            self.lePortInput.setEnabled(True)

    # Helper function to set the window scale
    def setWindowScale(self, width, height):
        # Set up the geometry for the window
        self.setGeometry(0, 0, width, height)
        # Move the window to center
        qtRec = self.frameGeometry()  # Get the frame rectangle
        centerPoint = QtWidgets.QDesktopWidget().availableGeometry().center()  # Get the center point
        qtRec.moveCenter(centerPoint)  # Move the frame rectangle to the center point
        self.move(qtRec.topLeft())  # Move the window to match the frame rectangle

    # Handle the key press event
    def keyPressEvent(self, event):
        # Handle the key "down" event
        if not event.isAutoRepeat():
            # Check for keyboard input
            if event.key() == QtCore.Qt.Key_R:
                self.lblSendCmd.setText("S")
                self.workerClient.sendCmdSlot("S")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_W:
                self.lblSendCmd.setText("F-C")
                self.workerClient.sendCmdSlot("F-C")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_S:
                self.lblSendCmd.setText("B-C")
                self.workerClient.sendCmdSlot("B-C")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_A:
                self.lblSendCmd.setText("L-S")
                self.workerClient.sendCmdSlot("L-S")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_D:
                self.lblSendCmd.setText("R-S")
                self.workerClient.sendCmdSlot("R-S")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_Q:
                self.lblSendCmd.setText("F-L-C")
                self.workerClient.sendCmdSlot("F-L-C")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_E:
                self.lblSendCmd.setText("F-R-C")
                self.workerClient.sendCmdSlot("F-R-C")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_Z:
                self.lblSendCmd.setText("B-L-C")
                self.workerClient.sendCmdSlot("B-L-C")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_C:
                self.lblSendCmd.setText("B-R-C")
                self.workerClient.sendCmdSlot("B-R-C")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_X:
                self.lblSendCmd.setText("B-D")
                self.workerClient.sendCmdSlot("B-D")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_T:
                self.lblSendCmd.setText("F-A")
                self.workerClient.sendCmdSlot("F-A")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_Y:
                self.lblSendCmd.setText("F-D")
                self.workerClient.sendCmdSlot("F-D")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_G:
                self.lblSendCmd.setText("B-A")
                self.workerClient.sendCmdSlot("B-A")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_U:
                self.lblSendCmd.setText("F-L-A")
                self.workerClient.sendCmdSlot("F-L-A")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_I:
                self.lblSendCmd.setText("F-L-D")
                self.workerClient.sendCmdSlot("F-L-D")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_H:
                self.lblSendCmd.setText("B-L-A")
                self.workerClient.sendCmdSlot("B-L-A")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_J:
                self.lblSendCmd.setText("B-L-D")
                self.workerClient.sendCmdSlot("B-L-D")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_O:
                self.lblSendCmd.setText("F-R-A")
                self.workerClient.sendCmdSlot("F-R-A")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_P:
                self.lblSendCmd.setText("F-R-D")
                self.workerClient.sendCmdSlot("F-R-D")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_K:
                self.lblSendCmd.setText("B-R-A")
                self.workerClient.sendCmdSlot("B-R-A")
                self.bCmdSent = True
            elif event.key() == QtCore.Qt.Key_L:
                self.lblSendCmd.setText("B-R-D")
                self.workerClient.sendCmdSlot("B-R-D")
                self.bCmdSent = True

    # Handle the key release event
    def keyReleaseEvent(self, event):
        pass
        # if not event.isAutoRepeat() and self.bCmdSent:
        #     self.lblSendCmd.setText("Stop")
        #     self.workerClient.sendCmdSlot("S")
        #     self.bCmdSent = False

    # Handle the close event
    def closeEvent(self, event):
        # Update the connection status
        self.bConnected = False
        # Attempt to connect/disconnect
        self.workerClient.connectionSlot(self.serverIP, self.serverPort, self.bConnected)

    # Slot to handle the connect button click event
    @QtCore.pyqtSlot()
    def handleConnectionSlot(self):
        try:
            # Check for connection status
            if not self.bConnected:
                # Retrieve the server IP address and port number
                serverIP = self.leIPInput.text()
                # Check for invalid server IP address
                if not serverIP:
                    raise Exception("Invalid server IP address")
                portNum = int(self.lePortInput.text())
                # Update the server IP and port
                self.serverIP = serverIP
                self.serverPort = portNum

            # Update the connection status
            self.bConnected = not self.bConnected
            # Attempt to connect/disconnect
            self.workerClient.connectionSlot(self.serverIP, self.serverPort, self.bConnected)
            # Update the UI display
            self.updateUI()
        except Exception as e:
            # Create a message box to show user the error
            msgBox = QtWidgets.QMessageBox()
            msgBox.setIcon(QtWidgets.QMessageBox.Warning)
            msgBox.setWindowTitle("Exception")
            msgBox.setText("Invalid server IP and port inputs!")
            msgBox.setDetailedText("Error: " + str(e))
            msgBox.exec_()

    # Slot to handle the settings button clicked event
    @QtCore.pyqtSlot()
    def settingsBtnSlot(self):
        # Show the settings window
        if self.settingsWindow.exec():
            # Ask user whether want to restart server and client
            msgBox = QtWidgets.QMessageBox
            ret = msgBox.question(self, "Restart Server and Client", "Do you want to restart server and client?", msgBox.Yes | msgBox.No)
            bRestart = ret == msgBox.Yes

            # Send the settings to server
            self.workerClient.sendSettings(self.settingsWindow.ServerPortNum, self.settingsWindow.BufferSize,
                                           self.settingsWindow.EncodeQuality, self.settingsWindow.FrameRate,
                                           self.settingsWindow.CommFrameWidth, bRestart)

            # Restart the client if needed
            if bRestart:
                # restart after 3 second
                restartThread = threading.Timer(3, self.restart)
                restartThread.start()

    #  Function to restart the client
    def restart(self):
        # Restart the client
        os.execl(sys.executable, sys.executable, *sys.argv)


# Entry point for the client app
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    clientWindow = ClientWindow()
    clientWindow.show()
    sys.exit(app.exec_())