# This is the implementation of the server for the SYSC 4907 Vision Based Control Robotics project
import os
import sys
import time

import cv2
import communication, settings, motor_control
from PyQt5 import QtWidgets, QtGui, QtCore


# The ServerWindow class define the main window for the server
class ServerWindow(QtWidgets.QWidget):

    # Constructor
    def __init__(self):
        super().__init__()
        # Define some class variables
        self.bVidShowHide = False  # Hold the status for video feed

        # Set up the geometry for the window
        self.setWindowScale(1200, 68)
        self.setMinimumHeight(68)
        # Set up the window title
        self.setWindowTitle("Vision Based Control Robotics Server")

        # Set up the GPIO pins
        self.motorControl = motor_control.MotorControl()

        # Set up the UI
        self.setupUI()

        # Set up the video feed worker
        self.workerVideoFeed = VideoFeedWorker()
        self.workerVideoFeed.sigVideoFeed.connect(self.videoDisplay.setImageSlot)  # Connect the video feed signal
        self.videoDisplay.sigResizeVideo.connect(self.workerVideoFeed.scaleVideo)  # Connect the window resize signal
        self.workerVideoFeed.start()  # Start the video feed worker

        # Set up the settings window
        self.settingsWindow = settings.ServerSettingsWindow()

        # Set up the communication layer
        self.workerServer = communication.ServerWorker(self.settingsWindow.PortNum, self.settingsWindow.BufferSize)
        self.workerVideoFeed.sigVideoComm.connect(self.workerServer.videoFrameSlot)
        self.workerServer.sigClientAccepted.connect(self.clientConnSlot)
        self.workerServer.sigCmdReceived.connect(self.receiveCmdSlot)
        self.workerServer.sigSettingsReceived.connect(self.receiveSettingsSlot)
        self.workerServer.start()  # Start the video feed worker

        # Set up the video feed button event
        self.btnShowHideVideo.clicked.connect(self.showHideVideo)
        # Set up the settings button event
        self.btnSettings.clicked.connect(self.settingsBtnSlot)

        # Update the UI
        self.updateUI()

    # Helper function to set up the UI
    def setupUI(self):
        # Video Display
        self.videoDisplay = communication.VideoDisplay()

        # IP Address
        lblIPHint = QtWidgets.QLabel("IP Address:")
        lblIPHint.setStyleSheet("color: blue; ")
        self.lblIPDisplay = QtWidgets.QLabel()
        self.lblIPDisplay.setStyleSheet("font-size: 10pt; border: 1px solid gray;")
        # Port Number
        lblPortHint = QtWidgets.QLabel("Port Number:")
        lblPortHint.setStyleSheet("color: blue;")
        self.lblPortDisplay = QtWidgets.QLabel()
        self.lblPortDisplay.setStyleSheet("font-size: 10pt; border: 1px solid gray;")
        # Connections Status
        lblConnStatusHint = QtWidgets.QLabel("Connection Status:")
        lblConnStatusHint.setStyleSheet("color: blue;")
        self.lblConnStatusDisplay = QtWidgets.QLabel("No Connection")
        self.lblConnStatusDisplay.setStyleSheet("font-size: 10pt; border: 1px solid gray;")
        # Received command
        lblReceivedCmdHint = QtWidgets.QLabel("Received Command:")
        lblReceivedCmdHint.setStyleSheet("color: blue;")
        self.lblReceivedCmd = QtWidgets.QLabel()
        self.lblReceivedCmd.setStyleSheet("font-size: 10pt; border: 1px solid gray;")
        # Settings
        lblSettingsHint = QtWidgets.QLabel("Settings:")
        lblSettingsHint.setStyleSheet("color: blue;")
        self.btnSettings = QtWidgets.QPushButton()
        self.btnSettings.setIcon(QtGui.QIcon("icon/settings.png"))
        # Video Status
        lblVideoStatusHint = QtWidgets.QLabel("Video Status:")
        lblVideoStatusHint.setStyleSheet("color: blue;")
        self.btnShowHideVideo = QtWidgets.QPushButton()
        self.btnShowHideVideo.setIcon(QtGui.QIcon("icon/video_off.png"))

        # Set up the layout
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setContentsMargins(0, 0, 0, 8)
        self.gridLayout.addWidget(self.videoDisplay, 0, 0, 1, 8)
        self.gridLayout.addWidget(lblIPHint, 1, 1)
        self.gridLayout.addWidget(lblPortHint, 1, 2)
        self.gridLayout.addWidget(lblConnStatusHint, 1, 3)
        self.gridLayout.addWidget(lblReceivedCmdHint, 1, 4)
        self.gridLayout.addWidget(lblSettingsHint, 1, 5)
        self.gridLayout.addWidget(lblVideoStatusHint, 1, 6)
        self.gridLayout.addWidget(self.lblIPDisplay, 2, 1)
        self.gridLayout.addWidget(self.lblPortDisplay, 2, 2)
        self.gridLayout.addWidget(self.lblConnStatusDisplay, 2, 3)
        self.gridLayout.addWidget(self.lblReceivedCmd, 2, 4)
        self.gridLayout.addWidget(self.btnSettings, 2, 5)
        self.gridLayout.addWidget(self.btnShowHideVideo, 2, 6)
        # Hide the video display on default
        self.videoDisplay.hide()
        # Row and column stretch
        self.gridLayout.setRowStretch(0, 1)
        self.gridLayout.setColumnStretch(1, 10)
        self.gridLayout.setColumnStretch(2, 10)
        self.gridLayout.setColumnStretch(3, 10)
        self.gridLayout.setColumnStretch(4, 10)
        self.gridLayout.setColumnStretch(5, 5)
        self.gridLayout.setColumnStretch(6, 5)
        # Set layout
        self.setLayout(self.gridLayout)

    # Update the UI
    # @param strCmd = String direction command to be displayed
    def updateUI(self, strCmd=""):
        self.lblIPDisplay.setText(self.workerServer.serverIP)
        self.lblPortDisplay.setText(str(self.workerServer.serverPort))
        # Check for the client IP
        if self.workerServer.clientAddr is None:
            # Client disconnected
            self.lblConnStatusDisplay.setText("No Connection")
        else:
            # Client Connected
            self.lblConnStatusDisplay.setText("Connected: " + self.workerServer.clientAddr[0])
        self.lblReceivedCmd.setText(strCmd)

    # Slot for the video button click event
    @QtCore.pyqtSlot()
    def showHideVideo(self):
        # Check the video feed status
        if self.bVidShowHide:
            # If the video is showing, turn off the video
            self.workerVideoFeed.stopVideo()
            self.bVidShowHide = False
        else:
            # If the video is not showing, turn on the video
            self.workerVideoFeed.startVideo()
            self.bVidShowHide = True
        # Check the video feed status
        if self.bVidShowHide:
            self.videoDisplay.show()  # Show the video display label
            self.btnShowHideVideo.setIcon(QtGui.QIcon("icon/video_on.png"))  # Update the icon for video feed button
            self.setWindowScale(1200, 700)  # Resize the window
        else:
            self.btnShowHideVideo.setIcon(QtGui.QIcon("icon/video_off.png"))  # Update the icon for video feed button
            self.videoDisplay.hide()  # Hide the video display label
            self.setWindowScale(1200, 68)  # Resize the window

    # Slot to handle client connection
    @QtCore.pyqtSlot()
    def clientConnSlot(self):
        # Update the UI
        self.updateUI()

    # Slot to receive direction command
    # @param strCmd = String command
    @QtCore.pyqtSlot(object)
    def receiveCmdSlot(self, strCmd):
        # Update the UI
        self.updateUI(strCmd)
        # TODO: Output the GPIO pins to control the movement
        self.motorControl.motionControl(strCmd)
        # TODO: delete the following print statement for debug
        print("PWM (Left, Right) = ("+str(self.motorControl.dutyCycleLeft) + ", " + str(self.motorControl.dutyCycleRight) + ")")

    # Slot to receive settings from the communication layer
    # @ param bRestartServer = flag for restart the server
    @QtCore.pyqtSlot(bool)
    def receiveSettingsSlot(self, bRestartServer):
        # Update the settings in the settings window
        self.settingsWindow.PortNum = self.workerServer.serverPort
        self.settingsWindow.BufferSize = self.workerServer.bufferSize
        self.settingsWindow.EncodeQuality = self.workerServer.encodeQuality
        self.settingsWindow.CamFrameRate = self.workerServer.frameRate
        self.settingsWindow.CommFrameRate = self.workerServer.frameRate
        self.settingsWindow.CommFrameWidth = self.workerServer.frameWidth
        # Save the settings to the xml file
        self.settingsWindow.saveXML()

        # Check if need to restart server
        if bRestartServer:
            self.restart()

    # Slot to handle the settings button clicked event
    @QtCore.pyqtSlot()
    def settingsBtnSlot(self):
        # Show the settings window
        if self.settingsWindow.exec():
            # Update the server settings
            self.workerServer.updateSettings(self.settingsWindow.EncodeQuality, self.settingsWindow.CommFrameRate, self.settingsWindow.CommFrameWidth)
            # Ask user if want to restart server
            msgBox = QtWidgets.QMessageBox
            ret = msgBox.question(self, "Restart Server", "Do you want to restart the server?", msgBox.Yes | msgBox.No)
            if ret == msgBox.Yes:
                # Restart the server
                self.restart()

    # Function to restart the server
    def restart(self):
        # Restart the server
        os.execl(sys.executable, sys.executable, *sys.argv)

    # Handle the close event
    def closeEvent(self, event):
        # TODO: Clean up  the GPIO pin
        self.motorControl.cleanGPIO()

    # Helper function to set the window scale
    def setWindowScale(self, width, height):
        # Set up the geometry for the window
        self.setGeometry(0, 0, width, height)
        # Move the window to center
        qtRec = self.frameGeometry()  # Get the frame rectangle
        centerPoint = QtWidgets.QDesktopWidget().availableGeometry().center()  # Get the center point
        qtRec.moveCenter(centerPoint)  # Move the frame rectangle to the center point
        self.move(qtRec.topLeft())  # Move the window to match the frame rectangle


# The VideoFeedWorker class implement the thread to feed video
class VideoFeedWorker(QtCore.QThread):

    # Signal for feeding the video
    sigVideoFeed = QtCore.pyqtSignal(QtGui.QImage)
    # Signal for sending the video to the communication layer (OpenCV frame)
    sigVideoComm = QtCore.pyqtSignal(object)
    # Frame-rate for the video
    frameRate = 30
    # Scale size for the video feed
    scaleSize = QtCore.QSize(640, 480)

    # Start the video feed thread
    def run(self):
        # Hold the status for show/hide video
        self.bShowVideo = False

        # Capture the video
        self.capture = cv2.VideoCapture(0)
        self.bCaptured = True

        # Hold the previous time
        prevTime = 0

        # Loop to capture the frame
        while True:
            # Calculate the time elapsed for frame rate
            timeElapsed = time.time() - prevTime
            # check for time
            if timeElapsed > 1./self.frameRate:
                # Store the previous time
                prevTime = time.time()
                # Capture the frame
                ret, frame = self.capture.read()
                # Emit the signal for video to be sent to the communication layer
                self.sigVideoComm.emit(frame)
                # Check the status for show/hide video
                if self.bShowVideo:
                    # Format the image
                    img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  # Convert to RGB
                    flippedImage = cv2.flip(img, 1)
                    # Convert image to Qt format
                    convertToQtFormat = QtGui.QImage(flippedImage.data, flippedImage.shape[1], flippedImage.shape[0], QtGui.QImage.Format_RGB888)
                    pic = convertToQtFormat.scaled(self.scaleSize, QtCore.Qt.KeepAspectRatio)
                    # Emit the signal for video display
                    self.sigVideoFeed.emit(pic)

    # Start the video feed
    def startVideo(self):
        self.bShowVideo = True

    # Stop the video feed
    def stopVideo(self):
        self.bShowVideo = False

    # Scale size for the video
    @QtCore.pyqtSlot(QtCore.QSize)
    def scaleVideo(self, scaleSize):
        self.scaleSize = scaleSize

    # Setter for the frame rate to retrieve image from camera
    def setFrameRate(self, camFrameRate):
        self.frameRate = camFrameRate


# Entry point for the server app
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    serverWindow = ServerWindow()
    serverWindow.show()
    sys.exit(app.exec_())


