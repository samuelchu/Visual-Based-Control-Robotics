# This file implemented the settings dialog box to change the communication settings
import sys
import xml.etree.ElementTree as ET
from xml.dom import minidom
from PyQt5 import QtWidgets, QtCore


# The ServerSettingsWindow define the settings window for the settings on server
class ServerSettingsWindow(QtWidgets.QDialog):
    # Filename for the server settings xml file
    SettingsFileName = "settings_server.xml"

    # Variables for holding the settings
    PortNum = 9999    # Port number for the server
    CamFrameRate = 30    # Camera reading frame rate
    BufferSize = 65536   # Buffer size
    EncodeQuality = 80    # Encode quality for the frame
    CommFrameRate = 30    # Communication frame rate
    CommFrameWidth = 400   # Frame width to be resized into before sending the frame

    # Constructor
    def __init__(self):
        super(ServerSettingsWindow, self).__init__()
        # Set up the geometry for the window
        self.setGeometry(0, 0, 500, 300)
        # Move the window to center
        qtRec = self.frameGeometry()  # Get the frame rectangle
        centerPoint = QtWidgets.QDesktopWidget().availableGeometry().center()  # Get the center point
        qtRec.moveCenter(centerPoint)  # Move the frame rectangle to the center point
        self.move(qtRec.topLeft())  # Move the window to match the frame rectangle
        # Set up the window title
        self.setWindowTitle("Settings")

        # Set up the UI
        self.setupUI()

        # Connect the button signal
        self.btnsDialog.rejected.connect(self.reject)
        self.btnsDialog.accepted.connect(self.saveFileSlot)

        # Read the settings from the xml file
        self.readXML()

    # Helper function to setup the UI
    def setupUI(self):
        # Server Settings
        self.gbServerSettings = QtWidgets.QGroupBox("Server Settings")
        self.flServerSettings = QtWidgets.QFormLayout()
        # Port number
        self.lePortNumber = QtWidgets.QLineEdit()
        self.flServerSettings.addRow(QtWidgets.QLabel("Port Number:"), self.lePortNumber)
        portNumHint = QtWidgets.QLabel("*Please note server need to be restarted to update port number")
        portNumHint.setStyleSheet("font-size: 8pt; color: red")
        self.flServerSettings.addRow(portNumHint)
        # Camera frame rate
        self.leCamFrameRate = QtWidgets.QLineEdit()
        self.flServerSettings.addRow(QtWidgets.QLabel("Camera Frame Rate:"), self.leCamFrameRate)

        # Communication Settings
        self.gbCommSettings = QtWidgets.QGroupBox("Communication Settings")
        self.flCommSettings = QtWidgets.QFormLayout()
        # Buffer size
        self.leBuffSize = QtWidgets.QLineEdit()
        self.flCommSettings.addRow(QtWidgets.QLabel("Buffer Size:"), self.leBuffSize)
        bufferSizeHint = QtWidgets.QLabel("*Please note both server and client need to be restarted to update buffer size")
        bufferSizeHint.setStyleSheet("font-size: 8pt; color: red")
        self.flCommSettings.addRow(bufferSizeHint)
        # Encode quality
        self.sbEncodeQuality = QtWidgets.QSpinBox()
        self.sbEncodeQuality.setSuffix("%")
        self.sbEncodeQuality.setMinimum(1)
        self.sbEncodeQuality.setMaximum(100)
        self.flCommSettings.addRow(QtWidgets.QLabel("Encode Quality:"), self.sbEncodeQuality)
        # Communication frame rate
        self.leCommFrameRate = QtWidgets.QLineEdit()
        self.flCommSettings.addRow(QtWidgets.QLabel("Communication Frame Rate:"), self.leCommFrameRate)
        # Communication frame width
        self.leCommFrameWidth = QtWidgets.QLineEdit()
        self.flCommSettings.addRow(QtWidgets.QLabel("Communication Frame Width:"), self.leCommFrameWidth)

        # Set up the dialog buttons
        self.btnsDialog = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel)

        # Setup the layouts
        self.gbServerSettings.setLayout(self.flServerSettings)
        self.gbCommSettings.setLayout(self.flCommSettings)
        # Root layout
        self.vblRoot = QtWidgets.QVBoxLayout()
        self.vblRoot.addWidget(self.gbServerSettings)
        self.vblRoot.addWidget(self.gbCommSettings)
        self.vblRoot.addWidget(self.btnsDialog)
        self.setLayout(self.vblRoot)

    # Function to update the UI display
    def updateUI(self):
        # Update the UI display based on the field values
        self.lePortNumber.setText(str(self.PortNum))
        self.leCamFrameRate.setText(str(self.CamFrameRate))
        self.leBuffSize.setText(str(self.BufferSize))
        self.sbEncodeQuality.setValue(self.EncodeQuality)
        self.leCommFrameRate.setText(str(self.CommFrameRate))
        self.leCommFrameWidth.setText(str(self.CommFrameWidth))

    # Slot for handling the save button
    @QtCore.pyqtSlot()
    def saveFileSlot(self):
        try:
            # Retrieve user input for port number
            try:
                portNum = int(self.lePortNumber.text())
                if portNum <= 0:
                    raise Exception()
            except:
                raise Exception("Invalid port number.")

            # Retrieve user input for camera frame rate
            try:
                camFrameRate = int(self.leCamFrameRate.text())
                if camFrameRate <= 0:
                    raise Exception()
            except:
                raise Exception("Invalid camera frame rate.")

            # Retrieve user input for buffer size
            try:
                bufferSize = int(self.leBuffSize.text())
                if bufferSize <= 0:
                    raise Exception()
            except:
                raise Exception("Invalid buffer size.")

            # Retrieve user input for encode quality
            try:
                encodeQuality = self.sbEncodeQuality.value()
                if encodeQuality <= 0 or encodeQuality > 100:
                    raise Exception()
            except:
                raise Exception("Invalid encode quality.")

            # Retrieve user input for communication frame rate
            try:
                commFrameRate = int(self.leCommFrameRate.text())
                if commFrameRate <= 0:
                    raise Exception()
            except:
                raise Exception("Invalid communication frame rate.")

            # Retrieve user input for communication frame width
            try:
                commFrameWidth = int(self.leCommFrameWidth.text())
                if commFrameWidth <= 0:
                    raise Exception()
            except:
                raise Exception("Invalid communication frame width.")

            # If all user inputs are valid, update the fields
            self.PortNum = portNum
            self.CamFrameRate = camFrameRate
            self.BufferSize = bufferSize
            self.EncodeQuality = encodeQuality
            self.CommFrameRate = commFrameRate
            self.CommFrameWidth = commFrameWidth

            # Save the xml file
            self.saveXML()

            # Execute the accept function
            self.accept()
        except Exception as e:
            # Inform user for invalid user input
            msgBox = QtWidgets.QMessageBox()
            msgBox.setIcon(QtWidgets.QMessageBox.Warning)
            msgBox.setWindowTitle("Invalid inputs")
            msgBox.setText(str(e))
            msgBox.exec_()

    # Handle the show event
    def showEvent(self, event):
        # Update the UI
        self.updateUI()

    # Read the xml file
    def readXML(self):
        try:
            # Read the settings xml file
            tree = ET.parse(self.SettingsFileName)
            root = tree.getroot()
            # Read the port number
            portNum = int(root.find(".//Server/PortNum").text)
            # Read the camera frame rate
            camFrameRate = int(root.find(".//Server/CamFrameRate").text)
            # Read the buffer size
            bufferSize = int(root.find(".//Communication/BufferSize").text)
            # Read the encode quality
            encodeQuality = int(root.find(".//Communication/EncodeQuality").text)
            # Read the communication frame rate
            commFrameRate = int(root.find(".//Communication/CommFrameRate").text)
            # Read the communication frame width
            commFrameWidth = int(root.find(".//Communication/CommFrameWidth").text)

            # If no exception so far, that mean the file is valid, save the settings
            self.PortNum = portNum
            self.CamFrameRate = camFrameRate
            self.BufferSize = bufferSize
            self.EncodeQuality = encodeQuality
            self.CommFrameRate = commFrameRate
            self.CommFrameWidth = commFrameWidth
        except:
            # If the file do not exist or corrupted, create/save the settings xml file with default value
            self.saveXML()

        # Update the UI display
        self.updateUI()

    # Function to save XML file
    def saveXML(self):
        # Create the root element
        root = ET.Element("Settings")
        # Create the server node
        serverNode = ET.SubElement(root, "Server")
        ET.SubElement(serverNode, "PortNum").text = str(self.PortNum)
        ET.SubElement(serverNode, "CamFrameRate").text = str(self.CamFrameRate)
        # Create the communication node
        commNode = ET.SubElement(root, "Communication")
        ET.SubElement(commNode, "BufferSize").text = str(self.BufferSize)
        ET.SubElement(commNode, "EncodeQuality").text = str(self.EncodeQuality)
        ET.SubElement(commNode, "CommFrameRate").text = str(self.CommFrameRate)
        ET.SubElement(commNode, "CommFrameWidth").text = str(self.CommFrameWidth)

        # Pretty print and save the xml file
        xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent="    ")
        with open(self.SettingsFileName, "w+") as f:
            f.write(xmlstr)
            f.close()


# The ClientSettingsWindow define the settings window for the settings on client
class ClientSettingsWindow(QtWidgets.QDialog):

    # Filename for the client settings xml file
    SettingsFileName = "settings_client.xml"

    # Variables for holding the settings
    ServerIPAddr = "192.168.0.14"  # Server IP address
    ServerPortNum = 9999  # Server port number
    BufferSize = 65536  # Buffer size
    EncodeQuality = 80  # Encode quality for the frame
    FrameRate = 30  # camera and communication frame rate
    CommFrameWidth = 400  # Frame width to be resized into before sending the frame

    # Constructor
    def __init__(self):
        super(ClientSettingsWindow, self).__init__()
        # Set up the geometry for the window
        self.setGeometry(0, 0, 500, 300)
        # Move the window to center
        qtRec = self.frameGeometry()  # Get the frame rectangle
        centerPoint = QtWidgets.QDesktopWidget().availableGeometry().center()  # Get the center point
        qtRec.moveCenter(centerPoint)  # Move the frame rectangle to the center point
        self.move(qtRec.topLeft())  # Move the window to match the frame rectangle
        # Set up the window title
        self.setWindowTitle("Settings")

        # Set up the UI
        self.setupUI()

        # Connect the button signal
        self.btnsDialog.rejected.connect(self.reject)
        self.btnsDialog.accepted.connect(self.saveFileSlot)

        # Read the settings from the xml file
        self.readXML()

    # Helper function to set up the UI
    def setupUI(self):
        # Client Settings
        self.gbClientSettings = QtWidgets.QGroupBox("Client Settings")
        self.flClientSettings = QtWidgets.QFormLayout()
        clientSettingsHint = QtWidgets.QLabel("*Please note client need to be restarted to update the client settings")
        clientSettingsHint.setStyleSheet("font-size: 8pt; color: red")
        self.flClientSettings.addRow(clientSettingsHint)
        # Server IP address
        self.leServerIPAddr = QtWidgets.QLineEdit()
        self.flClientSettings.addRow(QtWidgets.QLabel("Server IP Address:"), self.leServerIPAddr)
        # Server port number
        self.leServerPortNum = QtWidgets.QLineEdit()
        self.flClientSettings.addRow(QtWidgets.QLabel("Server Port Number:"), self.leServerPortNum)
        # Buffer Size
        self.leBufferSize = QtWidgets.QLineEdit()
        self.flClientSettings.addRow(QtWidgets.QLabel("Buffer Size:"), self.leBufferSize)

        # Server Settings
        self.gbServerSettings = QtWidgets.QGroupBox("Server Settings")
        self.flServerSettings = QtWidgets.QFormLayout()
        # Encode quality
        self.sbEncodeQuality = QtWidgets.QSpinBox()
        self.sbEncodeQuality.setSuffix("%")
        self.sbEncodeQuality.setMinimum(1)
        self.sbEncodeQuality.setMaximum(100)
        self.flServerSettings.addRow(QtWidgets.QLabel("Encode Quality:"), self.sbEncodeQuality)
        # Frame rate
        self.leFrameRate = QtWidgets.QLineEdit()
        self.flServerSettings.addRow(QtWidgets.QLabel("Frame Rate:"), self.leFrameRate)
        # Frame width
        self.leCommFrameWidth = QtWidgets.QLineEdit()
        self.flServerSettings.addRow(QtWidgets.QLabel("Communication Frame Width:"), self.leCommFrameWidth)

        # Set up the dialog buttons
        self.btnsDialog = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel)

        # Set up the layout
        self.gbClientSettings.setLayout(self.flClientSettings)
        self.gbServerSettings.setLayout(self.flServerSettings)
        # Root layout
        self.vblRoot = QtWidgets.QVBoxLayout()
        self.vblRoot.addWidget(self.gbClientSettings)
        self.vblRoot.addWidget(self.gbServerSettings)
        self.vblRoot.addWidget(self.btnsDialog)
        self.setLayout(self.vblRoot)

    # Function to update the UI display
    def updateUI(self):
        # Update the UI display based on the field values
        self.leServerIPAddr.setText(self.ServerIPAddr)
        self.leServerPortNum.setText(str(self.ServerPortNum))
        self.leBufferSize.setText(str(self.BufferSize))
        self.sbEncodeQuality.setValue(self.EncodeQuality)
        self.leFrameRate.setText(str(self.FrameRate))
        self.leCommFrameWidth.setText(str(self.CommFrameWidth))

    # Slot for handling the save button
    @QtCore.pyqtSlot()
    def saveFileSlot(self):
        try:
            # Retrieve user input for server IP address
            serverIPAddr = self.leServerIPAddr.text()

            # Retrieve user input for server port number
            try:
                serverPortNum = int(self.leServerPortNum.text())
                if serverPortNum <= 0:
                    raise Exception()
            except:
                raise Exception("Invalid server port number.")

            # Retrieve user input for buffer size
            try:
                bufferSize = int(self.leBufferSize.text())
                if bufferSize <= 0:
                    raise Exception()
            except:
                raise Exception("Invalid buffer size.")

            # Retrieve user input for encode quality
            try:
                encodeQuality = self.sbEncodeQuality.value()
                if encodeQuality <= 0 or encodeQuality > 100:
                    raise Exception()
            except:
                raise Exception("Invalid encode quality.")

            # Retrieve user input for frame rate
            try:
                frameRate = int(self.leFrameRate.text())
                if frameRate <= 0:
                    raise Exception()
            except:
                raise Exception("Invalid frame rate.")

            # Retrieve user input for communication frame width
            try:
                commFrameWidth = int(self.leCommFrameWidth.text())
                if commFrameWidth <= 0:
                    raise Exception()
            except:
                raise Exception("Invalid communication frame width.")

            # If all user inputs are valid, update the fields
            self.ServerIPAddr = serverIPAddr
            self.ServerPortNum = serverPortNum
            self.BufferSize = bufferSize
            self.EncodeQuality = encodeQuality
            self.FrameRate = frameRate
            self.CommFrameWidth = commFrameWidth

            # Save the xml file
            self.saveXML()

            # Execute the accept function
            self.accept()
        except Exception as e:
            # Inform user for invalid user input
            msgBox = QtWidgets.QMessageBox()
            msgBox.setIcon(QtWidgets.QMessageBox.Warning)
            msgBox.setWindowTitle("Invalid inputs")
            msgBox.setText(str(e))
            msgBox.exec_()

    # Handle the show event
    def showEvent(self, event):
        # Update the UI
        self.updateUI()

    # Read the xml file
    def readXML(self):
        try:
            # Read the settings xml file
            tree = ET.parse(self.SettingsFileName)
            root = tree.getroot()
            # Read server IP address
            serverIPAddr = root.find(".//Client/ServerIPAddr").text
            # Read the server port number
            serverPortNum = int(root.find(".//Client/ServerPortNum").text)
            # Read the buffer size
            bufferSize = int(root.find(".//Client/BufferSize").text)
            # Read encode quality
            encodeQuality = int(root.find(".//Server/EncodeQuality").text)
            # Read the frame rate
            frameRate = int(root.find(".//Server/FrameRate").text)
            # Read the communication frame width
            commFrameWidth = int(root.find(".//Server/CommFrameWidth").text)

            # If no exception so far, that mean the file is valid, save the settings
            self.ServerIPAddr = serverIPAddr
            self.ServerPortNum = serverPortNum
            self.BufferSize = bufferSize
            self.EncodeQuality = encodeQuality
            self.FrameRate = frameRate
            self.CommFrameWidth = commFrameWidth
        except:
            # If the file do not exist or corrupted, create/save the settings xml file with default value
            self.saveXML()

        # Update the UI display
        self.updateUI()

    # Function to save XML file
    def saveXML(self):
        # Create the root element
        root = ET.Element("Settings")
        # Create the client node
        clientNode = ET.SubElement(root, "Client")
        ET.SubElement(clientNode, "ServerIPAddr").text = self.ServerIPAddr
        ET.SubElement(clientNode, "ServerPortNum").text = str(self.ServerPortNum)
        ET.SubElement(clientNode, "BufferSize").text = str(self.BufferSize)
        # Create the server node
        serverNode = ET.SubElement(root, "Server")
        ET.SubElement(serverNode, "EncodeQuality").text = str(self.EncodeQuality)
        ET.SubElement(serverNode, "FrameRate").text = str(self.FrameRate)
        ET.SubElement(serverNode, "CommFrameWidth").text = str(self.CommFrameWidth)

        # Pretty print and save the xml file
        xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent="    ")
        with open(self.SettingsFileName, "w+") as f:
            f.write(xmlstr)
            f.close()


# Entry point for the settings window
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    clientSettingsWindow = ClientSettingsWindow()
    clientSettingsWindow.show()
    sys.exit(app.exec_())