# This file is an empty motor control class for testing purposes

class MotorControl:

    # Contructor
    def __init__(self):
        # Initialize the duty cycle for the GPIO pins (0-100)
        self.dutyCycleLeft = 0  # Duty cycle for the left two wheels
        self.dutyCycleRight = 0  # Duty cycle for the right two wheels
        self.linearStep = 10  # Step for the duty cycle in forward and backward directions
        self.diffStep = 30  # Step difference for turning

    # Helper function to change the speed (duty cycle)
    # @param dutyCycleLeft = duty cycle for the left 2 wheels
    #        dutyCycleRight = duty cycle for the right 2 wheels
    def changeDutyCycle(self, dutyCycleLeft, dutyCycleRight):
        pass

    # Function to clear the GPIO pins
    def cleanGPIO(self):
        pass

    # Handle the direction commands for GPIO pins
    # @ param  strCmd - String command for the direction
    def motionControl(self, strCmd):
        pass