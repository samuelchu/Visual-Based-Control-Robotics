# This file implements the communication layer for the SYSC 4907 Vision Based Control Robotics project
import socket
import time

import cv2, imutils, numpy, base64
from PyQt5 import QtCore, QtGui, QtWidgets

# Declare some global "constants"
CONN_MSG = 0b11111111  # Connection message
DISCONN_MSG = 0b11111110  # Disconnect message
SETTING_MSG = 0b11111101  # Settings message


# Function to encode the direction command TODO: change this function after finalized the command code
# @param strCmd = String command
# @return bit command (1 byte)
def encodeCmd(strCmd):
    # No left or right
    if strCmd == "S":
        return 0
    elif strCmd == "F-A":
        return 1
    elif strCmd == "F-C":
        return 2
    elif strCmd == "F-D":
        return 3
    elif strCmd == "B-A":
        return 4
    elif strCmd == "B-C":
        return 5
    elif strCmd == "B-D":
        return 6
    # Left
    elif strCmd == "L-S":
        return 7
    elif strCmd == "F-L-A":
        return 8
    elif strCmd == "F-L-C":
        return 9
    elif strCmd == "F-L-D":
        return 10
    elif strCmd == "B-L-A":
        return 11
    elif strCmd == "B-L-C":
        return 12
    elif strCmd == "B-L-D":
        return 13
    # Right
    elif strCmd == "R-S":
        return 14
    elif strCmd == "F-R-A":
        return 15
    elif strCmd == "F-R-C":
        return 16
    elif strCmd == "F-R-D":
        return 17
    elif strCmd == "B-R-A":
        return 18
    elif strCmd == "B-R-C":
        return 19
    elif strCmd == "B-R-D":
        return 20
        
        
# Function to decode the direction command TODO: change this function after finalized the command code
# @param bitCmd = Bit command (1 byte)
# @return String command
def decodeCmd(bitCmd):
    # No left and right
    if bitCmd == 0:
        return "S"
    elif bitCmd == 1:
        return "F-A"
    elif bitCmd == 2:
        return "F-C"
    elif bitCmd == 3:
        return "F-D"
    elif bitCmd == 4:
        return "B-A"    
    elif bitCmd == 5:
        return "B-C"
    elif bitCmd == 6:
        return "B-D"
    # Left
    elif bitCmd == 7:
        return "L-S"
    elif bitCmd == 8:
        return "F-L-A"
    elif bitCmd == 9:
        return "F-L-C"
    elif bitCmd == 10:
        return "F-L-D"
    elif bitCmd == 11:
        return "B-L-A"
    elif bitCmd == 12:
        return "B-L-C"
    elif bitCmd == 13:
        return "B-L-D"
    # Right
    elif bitCmd == 14:
        return "R-S"
    elif bitCmd == 15:
        return "F-R-A"
    elif bitCmd == 16:
        return "F-R-C"
    elif bitCmd == 17:
        return "F-R-D"
    elif bitCmd == 18:
        return "B-R-A"
    elif bitCmd == 19:
        return "B-R-C"
    elif bitCmd == 20:
        return "B-R-D"
        

# The ServerWorker class implements the socket for server
class ServerWorker(QtCore.QThread):

    # Signal for the accepted client socket
    sigClientAccepted = QtCore.pyqtSignal()
    # Signal for the received direction instruction (string)
    sigCmdReceived = QtCore.pyqtSignal(object)
    # Signal for settings received (boolean for restart server)
    sigSettingsReceived = QtCore.pyqtSignal(bool)

    # Declare some variables
    clientAddr = None  # Hold the client connection status
    bShowVideo = False  # Hold the status for show/hide video
    serverIP = ""  # Server IP address
    serverPort = 9999  # Server port number
    videoFrame = None  # Video frame to be sent
    frameRate = 30  # Frame rate for the video for sending
    frameWidth = 400  # Frame width for the video to resize to before sending
    bufferSize = 65536  # Buffer size for the video (send buffer size for server, receive buffer size for client)
    encodeQuality = 80  # Encode quality for the image, maximum 100%

    # Constructor to set up the server socket
    # @param portNum = server port number
    def __init__(self, portNum=9999, buffSize=65536):
        super(ServerWorker, self).__init__()
        #Store the parameters
        self.serverPort = portNum
        self.bufferSize = buffSize
        # Initialize the server socket using UDP protocol
        self.sockServer = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sockServer.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, self.bufferSize)  # Send buffer size
        self.serverIP = socket.gethostbyname(socket.gethostname()+".local")  # Get the server IP
        # Bind the server address
        self.sockServer.bind((self.serverIP, self.serverPort))

        # Set up the receive message worker
        self.workReceiveMsg = ReceiveMsgWorker(self.sockServer)
        self.workReceiveMsg.sigMsgReceived.connect(self.receiveMsgSlot)
        self.workReceiveMsg.sigSettingsReceived.connect(self.receiveSettingsSlot)
        self.workReceiveMsg.start()  # Start the receive message worker

    # Run function for the thread
    def run(self):
        # Loop for handling the server thread
        while True:
            # Hold the previous time for frame rate calculation
            prevTime = 0
            # Start the loop for sending video frame
            while self.clientAddr is not None:
                # Calculate the time elapsed
                timeElapsed = time.time() - prevTime
                if timeElapsed > 1./self.frameRate:
                    # Update the previous time
                    prevTime = time.time()

                    # Resize the video frame
                    frame = imutils.resize(self.videoFrame, width=self.frameWidth)
                    # Encode the video frame
                    encoded, buffer = cv2.imencode(".jpg", frame, [cv2.IMWRITE_JPEG_QUALITY, self.encodeQuality])
                    # Encode the message
                    msg = base64.b64encode(buffer)
                try:
                    # Send to the client
                    self.sockServer.sendto(msg, self.clientAddr)
                except Exception as e:
                    print(e)

    # Slot for the video frame
    @QtCore.pyqtSlot(object)
    def videoFrameSlot(self, image):
        # Update the video frame to be sent
        self.videoFrame = image

    # Slot to handle the received message
    # @param msg = received message (8-bits)
    # @param clientAddr = client address
    @QtCore.pyqtSlot(int, object)
    def receiveMsgSlot(self, msg, clientAddr):
        # Check the message
        if msg == CONN_MSG:
            # Save the client address
            self.clientAddr = clientAddr
            # Emit the client accepted signal
            self.sigClientAccepted.emit()
        elif msg == DISCONN_MSG:
            # Empty the client address
            self.clientAddr = None
            # Emit the client accepted signal
            self.sigClientAccepted.emit()
        else:
            # Decode the direction instruction
            strCmd = decodeCmd(msg)
            # Emit the signal for the direction instruction
            self.sigCmdReceived.emit(strCmd)

    # Slot to handle the received settings
    # @param arrData = bytes data for the settings
    @QtCore.pyqtSlot(bytes)
    def receiveSettingsSlot(self, arrData):
        # Decode and update the settings
        # Decode and update the port number, which is encoded in 2 bytes
        self.serverPort = int.from_bytes(arrData[:2], byteorder="big")
        # Decode and update the buffer size, which is encoded in 3 bytes
        self.bufferSize = int.from_bytes(arrData[2:5], byteorder="big")
        # Decode and update the encode quality, which is encoded in 1 byte
        self.encodeQuality = int.from_bytes(arrData[5:6], byteorder="big")
        # Decode and update frame rate, which is encoded in 1 byte
        self.frameRate = int.from_bytes(arrData[6:7], byteorder="big")
        # Decode and update communication frame width, which is encoded in 2 bytes
        self.frameWidth = int.from_bytes(arrData[7:9], byteorder="big")

        # Decode the restart server flag, which is encoded in 1 byte
        bRestartServer = (int.from_bytes(arrData[9:], byteorder="big") == 1)

        # Emit the signal for settings received
        self.sigSettingsReceived.emit(bRestartServer)

    # Function for update server settings
    # @param encodeQuality - encode quality for the image to send
    #        commFrameRate - frame rate for the image to send
    #        commFrameWidth - frame width for the image to send
    def updateSettings(self, encodeQuality, commFrameRate, commFrameWidth):
        # Update encode quality
        self.encodeQuality = encodeQuality
        self.frameRate = commFrameRate
        self.frameWidth = commFrameWidth


# The ReceiveMsgWorker class is a helper class for the server socket to receive messages
class ReceiveMsgWorker(QtCore.QThread):

    # Signal for the received message (8-bits, String)
    sigMsgReceived = QtCore.pyqtSignal(int, object)
    # Signal for the received settings (bytes)
    sigSettingsReceived = QtCore.pyqtSignal(bytes)

    # Constructor
    # @param objSocket - socket object to receive from
    def __init__(self, objSocket):
        super(ReceiveMsgWorker, self).__init__()
        # Store the socket object
        self.objSocket = objSocket

    # Run function for the thread
    def run(self):
        # Loop for handling the receive message thread
        while True:
            try:
                # Receive 1 byte from the socket
                msg, clientAddr = self.objSocket.recvfrom(1)
                # Convert byte to int
                msg = int.from_bytes(msg, byteorder="big")

                # Check for settings message
                if msg == SETTING_MSG:
                    # Retrieve the byte array
                    arrData, _ = self.objSocket.recvfrom(16)
                    # Emit the settings signal
                    self.sigSettingsReceived.emit(arrData)
                else:
                    # Emit the message signal
                    self.sigMsgReceived.emit(msg, clientAddr)
            except:
                pass


# The ClientWorker class implements the socket for the client
class ClientWorker(QtCore.QThread):

    # Signal for the video frame received from the server (OpenCV frame)
    sigVideoComm = QtCore.pyqtSignal(object)
    # Signal for feeding the video
    sigVideoFeed = QtCore.pyqtSignal(QtGui.QImage)
    # Scale size for the video feed
    scaleSize = QtCore.QSize(1280, 720)

    # Declare some fields
    bConnStatus = False  # Connection status
    bufferSize = 65536  # Buffer size for the video (send buffer size for server, receive buffer size for client)

    # Constructor to set up the client socket
    def __init__(self, buffSize=65536):
        super(ClientWorker, self).__init__()
        # Save the input parameters
        self.bufferSize = buffSize
        # Initialize the client socket using UDP protocol
        self.sockClient = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sockClient.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, self.bufferSize)  # Receive buffer size

    # Run function for the thread
    def run(self):
        # Loop for handling the client thread
        while True:
            # Check for connection status
            if self.bConnStatus:
                try:
                    # Receive data from the socket
                    packet, _ = self.sockClient.recvfrom(self.bufferSize)
                    # TODO: delete this print after debug
                    print(len(packet))
                    # Decode the data
                    data = base64.b64decode(packet, " /")
                    npData = numpy.fromstring(data, dtype=numpy.uint8)
                    # Convert to OpenCV frame
                    frame = cv2.imdecode(npData, 1)
                    # Emit the signal for the frame received from the server
                    self.sigVideoComm.emit(frame)
                    # Format the image for video feed signal
                    img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  # Convert to RGB
                    flippedImage = cv2.flip(img, 1)
                    # Convert image to Qt format
                    convertToQtFormat = QtGui.QImage(flippedImage.data, flippedImage.shape[1], flippedImage.shape[0], QtGui.QImage.Format_RGB888)
                    pic = convertToQtFormat.scaled(self.scaleSize, QtCore.Qt.KeepAspectRatio)
                    # Emit the signal for video display
                    self.sigVideoFeed.emit(pic)
                except:
                    pass

    # Slot for update the connection status
    # @param serverIP - Server IP address
    # @param serverPort - Server port number
    # @param bConn - True to connect to server, False to disconnect
    @QtCore.pyqtSlot(object, int, bool)
    def connectionSlot(self, serverIP, serverPort, bConn):
        # Store the server IP and port
        self.serverIP = serverIP
        self.serverPort = serverPort
        # Update the connection status
        self.bConnStatus = bConn
        # Check for connection status
        if self.bConnStatus:
            # Send the connection message to the server
            self.sockClient.sendto(CONN_MSG.to_bytes(1, byteorder="big"), (self.serverIP, self.serverPort))
        else:
            # Send the disconnect message to the server
            self.sockClient.sendto(DISCONN_MSG.to_bytes(1, byteorder="big"), (self.serverIP, self.serverPort))

    # Slot for scale size for the video feed
    @QtCore.pyqtSlot(QtCore.QSize)
    def scaleVideoSlot(self, scaleSize):
        self.scaleSize = scaleSize

    # Slot for sending the direction command
    # @param strCmd = string command
    @QtCore.pyqtSlot(object)
    def sendCmdSlot(self, strCmd):
        # Check for connection status
        if self.bConnStatus:
            # Encode the command
            bitCmd = encodeCmd(strCmd)
            # Send the bit command
            self.sockClient.sendto(bitCmd.to_bytes(1, byteorder="big"), (self.serverIP, self.serverPort))

    # Function to send server settings
    # @param  portNum = port number for the server
    #         bufferSize = buffer size for the server
    #         encodeQuality = encode quality for the image
    #         frameRate = camera and communication frame rate for the server
    #         commFrameWidth = frame width to resize the image before sending from the server
    #         bRestartServer = flag for restart server: True to restart, false otherwise
    def sendSettings(self, portNum, bufferSize, encodeQuality, frameRate, commFrameWidth, bRestartServer):
        # Send the settings message to the server
        self.sockClient.sendto(SETTING_MSG.to_bytes(1, byteorder="big"), (self.serverIP, self.serverPort))
        # Server port number encoded in 2 bytes, which gives max 2^16 - 1 = 65535
        bytePortNum = portNum.to_bytes(2, byteorder="big")
        # Buffer size encoded in 3 bytes, which gives max 2^24 - 1 = 16777215
        byteBufferSize = bufferSize.to_bytes(3, byteorder="big")
        # Encode quality encoded in 1 byte, which gives max 2^8 - 1= 255. Maximum encode quality should be 100%
        byteEncodeQuality = encodeQuality.to_bytes(1, byteorder="big")
        # Frame rate encoded in 1 byte, which give max 2^8 - 1 = 255
        byteFrameRate = frameRate.to_bytes(1, byteorder="big")
        # Communication frame width encoded in 2 bytes, which give max 2^16 - 1 = 65535
        byteCommFrameWidth = commFrameWidth.to_bytes(2, byteorder="big")
        # Encode restart server flag in 1 byte: 1 for true, 0 for false
        if bRestartServer:
            byteRestartServer = (1).to_bytes(1, byteorder="big")
        else:
            byteRestartServer = (0).to_bytes(1, byteorder="big")
        # Construct the byte array
        arrBytes = bytePortNum + byteBufferSize + byteEncodeQuality + byteFrameRate + byteCommFrameWidth + byteRestartServer

        # Send to the server
        self.sockClient.sendto(arrBytes, (self.serverIP, self.serverPort))


# The VideoDisplay class uses the QLabel for the video display
class VideoDisplay(QtWidgets.QLabel):

    # Signal for resize video
    sigResizeVideo = QtCore.pyqtSignal(QtCore.QSize)

    # Constructor
    def __init__(self):
        super(VideoDisplay, self).__init__()
        # Set up the UI
        self.setupUI()

    # Set up the UI for the video display
    def setupUI(self):
        # Black background
        self.setStyleSheet("background-color: black;")

        # Create the display label
        self.lblDisplay = QtWidgets.QLabel(self)
        vbLayout = QtWidgets.QVBoxLayout(self)
        vbLayout.setContentsMargins(0, 0, 0, 0)
        vbLayout.addWidget(self.lblDisplay, alignment=QtCore.Qt.AlignCenter)

    # Handle the resize event
    def resizeEvent(self, event):
        self.sigResizeVideo.emit(self.size())

    # Slot to set the image for the video display
    @QtCore.pyqtSlot(QtGui.QImage)
    def setImageSlot(self, image):
        self.lblDisplay.setPixmap(QtGui.QPixmap.fromImage(image))
