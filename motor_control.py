# The motor control class control the GPIO pins for the car movement

import RPi.GPIO as GPIO

# Constants for the GPIO pins control
in1 = 24 #back right
in2 = 23 #back right
in3 = 19 #back left
in4 = 13 #back left
in5 = 15 #front left
in6 = 18 #front left
in7 = 3 #front right
in8 = 2 #front right

enA = 25 #back right
enB = 26 #back left
enA1 = 14 #front left
enB1 = 4 #front right


class MotorControl:

    # Contructor
    def __init__(self):
        # Initialize the duty cycle for the GPIO pins (0-100)
        self.dutyCycleLeft = 0  # Duty cycle for the left two wheels
        self.dutyCycleRight = 0  # Duty cycle for the right two wheels
        self.linearStep = 10  # Step for the duty cycle in forward and backward directions
        self.diffStep = 30  # Step difference for turning

        # Setup the GPIO pins
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(in1, GPIO.OUT)
        GPIO.setup(in2, GPIO.OUT)
        GPIO.setup(in3, GPIO.OUT)
        GPIO.setup(in4, GPIO.OUT)
        GPIO.setup(enA, GPIO.OUT)
        GPIO.setup(enB, GPIO.OUT)
        GPIO.setup(in5, GPIO.OUT)
        GPIO.setup(in6, GPIO.OUT)
        GPIO.setup(in7, GPIO.OUT)
        GPIO.setup(in8, GPIO.OUT)
        GPIO.setup(enA1, GPIO.OUT)
        GPIO.setup(enB1, GPIO.OUT)

        # Default to stop
        GPIO.output(in1, GPIO.LOW)
        GPIO.output(in2, GPIO.LOW)
        GPIO.output(in3, GPIO.LOW)
        GPIO.output(in4, GPIO.LOW)
        GPIO.output(in5, GPIO.LOW)
        GPIO.output(in6, GPIO.LOW)
        GPIO.output(in7, GPIO.LOW)
        GPIO.output(in8, GPIO.LOW)

        # Hold the pins for speed
        self.pin1 = GPIO.PWM(enA1, 100)  # Front-left wheel
        self.pin2 = GPIO.PWM(enB1, 100)  # Front-right wheel
        self.pin3 = GPIO.PWM(enB, 100)  # Back-left wheel
        self.pin4 = GPIO.PWM(enA, 100)  # Back-right wheel

        # Start the pins with 0 duty cycle
        self.pin1.start(0)
        self.pin2.start(0)
        self.pin3.start(0)
        self.pin4.start(0)

    # Helper function to change the speed (duty cycle)
    # @param dutyCycleLeft = duty cycle for the left 2 wheels
    #        dutyCycleRight = duty cycle for the right 2 wheels
    def changeDutyCycle(self, dutyCycleLeft, dutyCycleRight):
        # Check for valid duty cycle for left
        if 100 >= dutyCycleLeft >= 0:
            # Update the field
            self.dutyCycleLeft = dutyCycleLeft
            # Update the duty cycle for the pins for the left side
            self.pin1.ChangeDutyCycle(self.dutyCycleLeft)
            self.pin3.ChangeDutyCycle(self.dutyCycleLeft)
        # Check for valid duty cycle for right
        if 100 >= dutyCycleRight >= 0:
            # Update the field
            self.dutyCycleRight = dutyCycleRight
            # Update the duty cycle for the pins for the right side
            self.pin2.ChangeDutyCycle(self.dutyCycleRight)
            self.pin4.ChangeDutyCycle(self.dutyCycleRight)

    # Function to clear the GPIO pins
    def cleanGPIO(self):
        GPIO.cleanup()

    # Handle the direction commands for GPIO pins
    # @ param  strCmd - String command for the direction
    def motionControl(self, strCmd):
        # Handle stop movement
        if strCmd == "S":
            # Output GPIO for stop movement
            GPIO.output(in1, GPIO.LOW)
            GPIO.output(in2, GPIO.LOW)
            GPIO.output(in3, GPIO.LOW)
            GPIO.output(in4, GPIO.LOW)
            GPIO.output(in5, GPIO.LOW)
            GPIO.output(in6, GPIO.LOW)
            GPIO.output(in7, GPIO.LOW)
            GPIO.output(in8, GPIO.LOW)
            # Update the duty cycle
            self.changeDutyCycle(0, 0)

        # Handle forward linear movement
        elif strCmd == "F-A" or strCmd == "F-C" or strCmd == "F-D":
            # Output GPIO for forward movement
            GPIO.output(in1, GPIO.HIGH)
            GPIO.output(in2, GPIO.LOW)
            GPIO.output(in3, GPIO.HIGH)
            GPIO.output(in4, GPIO.LOW)
            GPIO.output(in5, GPIO.HIGH)
            GPIO.output(in6, GPIO.LOW)
            GPIO.output(in7, GPIO.HIGH)
            GPIO.output(in8, GPIO.LOW)
            # Check for acceleration, constant, or deceleration
            if strCmd == "F-A":
                # Find the maximum duty cycle
                maxDutyCycle = max(self.dutyCycleLeft, self.dutyCycleRight)
                # Set both duty cycle to maximum
                self.dutyCycleLeft = maxDutyCycle
                self.dutyCycleRight = maxDutyCycle
                # Increase the duty cycle
                self.changeDutyCycle(self.dutyCycleLeft + self.linearStep, self.dutyCycleRight + self.linearStep)
            elif strCmd == "F-C":
                # Find the average duty cycle
                maxDutyCycle = (self.dutyCycleLeft + self.dutyCycleRight) / 2.0
                # Set both duty cycle to maximum
                self.dutyCycleLeft = maxDutyCycle
                self.dutyCycleRight = maxDutyCycle
                # Set the duty cycle
                self.changeDutyCycle(self.dutyCycleLeft, self.dutyCycleRight)
            elif strCmd == "F-D":
                # Find the minimum duty cycle
                minDutyCycle = min(self.dutyCycleLeft, self.dutyCycleRight)
                # Set both duty cycle to minimum
                self.dutyCycleLeft = minDutyCycle
                self.dutyCycleRight = minDutyCycle
                # Decrease the duty cycle
                self.changeDutyCycle(self.dutyCycleLeft - self.linearStep, self.dutyCycleRight - self.linearStep)

        # Handle backward linear movement
        elif strCmd == "B-A" or strCmd == "B-C" or strCmd == "B-D":
            # Output GPIO for backward
            GPIO.output(in1, GPIO.LOW)
            GPIO.output(in2, GPIO.HIGH)
            GPIO.output(in3, GPIO.LOW)
            GPIO.output(in4, GPIO.HIGH)
            GPIO.output(in5, GPIO.LOW)
            GPIO.output(in6, GPIO.HIGH)
            GPIO.output(in7, GPIO.LOW)
            GPIO.output(in8, GPIO.HIGH)
            # Check for acceleration, constant, or deceleration
            if strCmd == "B-A":
                # Find the maximum duty cycle
                maxDutyCycle = max(self.dutyCycleLeft, self.dutyCycleRight)
                # Set both duty cycle to maximum
                self.dutyCycleLeft = maxDutyCycle
                self.dutyCycleRight = maxDutyCycle
                # Increase the duty cycle
                self.changeDutyCycle(self.dutyCycleLeft + self.linearStep, self.dutyCycleRight + self.linearStep)
            elif strCmd == "B-C":
                # Find the average duty cycle
                maxDutyCycle = (self.dutyCycleLeft + self.dutyCycleRight) / 2.0
                # Set both duty cycle to maximum
                self.dutyCycleLeft = maxDutyCycle
                self.dutyCycleRight = maxDutyCycle
                # Set the duty cycle
                self.changeDutyCycle(self.dutyCycleLeft, self.dutyCycleRight)
            elif strCmd == "B-D":
                # Find the minimum duty cycle
                minDutyCycle = min(self.dutyCycleLeft, self.dutyCycleRight)
                # Set both duty cycle to minimum
                self.dutyCycleLeft = minDutyCycle
                self.dutyCycleRight = minDutyCycle
                # Decrease the duty cycle
                self.changeDutyCycle(self.dutyCycleLeft - self.linearStep, self.dutyCycleRight - self.linearStep)

        # Handle stop left turn
        elif strCmd == "L-S":
            # Output GPIO for stop left turn
            GPIO.output(in1, GPIO.HIGH)
            GPIO.output(in2, GPIO.LOW)
            GPIO.output(in3, GPIO.LOW)
            GPIO.output(in4, GPIO.HIGH)
            GPIO.output(in5, GPIO.LOW)
            GPIO.output(in6, GPIO.HIGH)
            GPIO.output(in7, GPIO.HIGH)
            GPIO.output(in8, GPIO.LOW)
            self.changeDutyCycle(self.diffStep/2, self.diffStep/2)

        # Handle stop right turn
        elif strCmd == "R-S":
            # Output GPIO for stop left turn
            GPIO.output(in1, GPIO.LOW)
            GPIO.output(in2, GPIO.HIGH)
            GPIO.output(in3, GPIO.HIGH)
            GPIO.output(in4, GPIO.LOW)
            GPIO.output(in5, GPIO.HIGH)
            GPIO.output(in6, GPIO.LOW)
            GPIO.output(in7, GPIO.LOW)
            GPIO.output(in8, GPIO.HIGH)
            self.changeDutyCycle(self.diffStep/2, self.diffStep/2)

        # Handle forward turn movement
        elif strCmd == "F-L-A" or strCmd == "F-L-C" or strCmd == "F-L-D" or strCmd == "F-R-A" or strCmd == "F-R-C" or strCmd == "F-R-D":
            # Output GPIO for forward movement
            GPIO.output(in1, GPIO.HIGH)
            GPIO.output(in2, GPIO.LOW)
            GPIO.output(in3, GPIO.HIGH)
            GPIO.output(in4, GPIO.LOW)
            GPIO.output(in5, GPIO.HIGH)
            GPIO.output(in6, GPIO.LOW)
            GPIO.output(in7, GPIO.HIGH)
            GPIO.output(in8, GPIO.LOW)
            # Check for forward left acceleration, constant, or deceleration
            if strCmd == "F-L-A":
                # Find the maximum duty cycle
                maxDutyCycle = max(self.dutyCycleLeft, self.dutyCycleRight)
                # Check if the maximum duty cycle plus the difference required is larger than 100 limit
                if maxDutyCycle + self.diffStep + self.linearStep > 100:
                    self.dutyCycleRight = 100
                    self.dutyCycleLeft = 100 - self.diffStep
                    # Set the duty cycle
                    self.changeDutyCycle(self.dutyCycleLeft, self.dutyCycleRight)
                else:
                    # Set the maximum duty cycle with difference
                    self.dutyCycleLeft = maxDutyCycle
                    self.dutyCycleRight = maxDutyCycle + self.diffStep
                    # Increase the duty cycle
                    self.changeDutyCycle(self.dutyCycleLeft + self.linearStep, self.dutyCycleRight + self.linearStep)
            elif strCmd == "F-L-C":
                # Find the maximum and minimum duty cycle
                maxDutyCycle = max(self.dutyCycleLeft, self.dutyCycleRight)
                minDutyCycle = min(self.dutyCycleLeft, self.dutyCycleRight)
                # Check if the maximum duty cycle plus the difference required is larger than 100 limit
                if maxDutyCycle + self.diffStep/2 > 100:
                    self.dutyCycleRight = 100
                    self.dutyCycleLeft = 100 - self.diffStep
                elif minDutyCycle - self.diffStep/2 < 0:
                    self.dutyCycleLeft = 0
                    self.dutyCycleRight = self.diffStep
                else:
                    # Set the maximum duty cycle with difference
                    self.dutyCycleLeft -= self.diffStep/2
                    self.dutyCycleRight += self.diffStep/2
                # Set the duty cycle
                self.changeDutyCycle(self.dutyCycleLeft, self.dutyCycleRight)
            elif strCmd == "F-L-D":
                # Find the minimum duty cycle
                minDutyCycle = min(self.dutyCycleLeft, self.dutyCycleRight)
                # Check if the minimum duty cycle minus the difference required is lower than 0 limit
                if minDutyCycle - self.diffStep - self.linearStep < 0:
                    self.dutyCycleRight = self.diffStep
                    self.dutyCycleLeft = 0
                    # Set the duty cycle
                    self.changeDutyCycle(self.dutyCycleLeft, self.dutyCycleRight)
                else:
                    # Set the minimum duty cycle with difference
                    self.dutyCycleLeft = minDutyCycle - self.diffStep
                    self.dutyCycleRight = minDutyCycle
                    # Decrease the duty cycle
                    self.changeDutyCycle(self.dutyCycleLeft - self.linearStep, self.dutyCycleRight - self.linearStep)
            # Check for forward right acceleration, constant, or deceleration
            elif strCmd == "F-R-A":
                # Find the maximum duty cycle
                maxDutyCycle = max(self.dutyCycleLeft, self.dutyCycleRight)
                # Check if the maximum duty cycle plus the difference required is larger than 100 limit
                if maxDutyCycle + self.diffStep + self.linearStep > 100:
                    self.dutyCycleRight = 100 - self.diffStep
                    self.dutyCycleLeft = 100
                    # Set the duty cycle
                    self.changeDutyCycle(self.dutyCycleLeft, self.dutyCycleRight)
                else:
                    # Set the maximum duty cycle with difference
                    self.dutyCycleLeft = maxDutyCycle + self.diffStep
                    self.dutyCycleRight = maxDutyCycle
                    # Increase the duty cycle
                    self.changeDutyCycle(self.dutyCycleLeft + self.linearStep, self.dutyCycleRight + self.linearStep)
            elif strCmd == "F-R-C":
                # Find the maximum and minimum duty cycle
                maxDutyCycle = max(self.dutyCycleLeft, self.dutyCycleRight)
                minDutyCycle = min(self.dutyCycleLeft, self.dutyCycleRight)
                # Check if the maximum duty cycle plus the difference required is larger than 100 limit
                if maxDutyCycle + self.diffStep/2 > 100:
                    self.dutyCycleRight = 100 - self.diffStep
                    self.dutyCycleLeft = 100
                elif minDutyCycle - self.diffStep/2 < 0:
                    self.dutyCycleLeft = self.diffStep
                    self.dutyCycleRight = 0
                else:
                    # Set the maximum duty cycle with difference
                    self.dutyCycleLeft += self.diffStep/2
                    self.dutyCycleRight -= self.diffStep/2
                # Set the duty cycle
                self.changeDutyCycle(self.dutyCycleLeft, self.dutyCycleRight)
            elif strCmd == "F-R-D":
                # Find the minimum duty cycle
                minDutyCycle = min(self.dutyCycleLeft, self.dutyCycleRight)
                # Check if the minimum duty cycle minus the difference required is lower than 0 limit
                if minDutyCycle - self.diffStep - self.linearStep < 0:
                    self.dutyCycleRight = 0
                    self.dutyCycleLeft = self.diffStep
                    # Set the duty cycle
                    self.changeDutyCycle(self.dutyCycleLeft, self.dutyCycleRight)
                else:
                    # Set the minimum duty cycle with difference
                    self.dutyCycleLeft = minDutyCycle
                    self.dutyCycleRight = minDutyCycle - self.diffStep
                    # Decrease the duty cycle
                    self.changeDutyCycle(self.dutyCycleLeft - self.linearStep, self.dutyCycleRight - self.linearStep)

        # Handle backward turn movement
        elif strCmd == "B-L-A" or strCmd == "B-L-C" or strCmd == "B-L-D" or strCmd == "B-R-A" or strCmd == "B-R-C" or strCmd == "B-R-D":
            # Output GPIO for backward
            GPIO.output(in1, GPIO.LOW)
            GPIO.output(in2, GPIO.HIGH)
            GPIO.output(in3, GPIO.LOW)
            GPIO.output(in4, GPIO.HIGH)
            GPIO.output(in5, GPIO.LOW)
            GPIO.output(in6, GPIO.HIGH)
            GPIO.output(in7, GPIO.LOW)
            GPIO.output(in8, GPIO.HIGH)
            # Check for backward left acceleration, constant, or deceleration
            if strCmd == "B-L-A":
                # Find the maximum duty cycle
                maxDutyCycle = max(self.dutyCycleLeft, self.dutyCycleRight)
                # Check if the maximum duty cycle plus the difference required is larger than 100 limit
                if maxDutyCycle + self.diffStep + self.linearStep > 100:
                    self.dutyCycleRight = 100
                    self.dutyCycleLeft = 100 - self.diffStep
                    # Set the duty cycle
                    self.changeDutyCycle(self.dutyCycleLeft, self.dutyCycleRight)
                else:
                    # Set the maximum duty cycle with difference
                    self.dutyCycleLeft = maxDutyCycle
                    self.dutyCycleRight = maxDutyCycle + self.diffStep
                    # Increase the duty cycle
                    self.changeDutyCycle(self.dutyCycleLeft + self.linearStep, self.dutyCycleRight + self.linearStep)
            elif strCmd == "B-L-C":
                # Find the maximum and minimum duty cycle
                maxDutyCycle = max(self.dutyCycleLeft, self.dutyCycleRight)
                minDutyCycle = min(self.dutyCycleLeft, self.dutyCycleRight)
                # Check if the maximum duty cycle plus the difference required is larger than 100 limit
                if maxDutyCycle + self.diffStep/2 > 100:
                    self.dutyCycleRight = 100
                    self.dutyCycleLeft = 100 - self.diffStep
                elif minDutyCycle - self.diffStep/2 < 0:
                    self.dutyCycleLeft = 0
                    self.dutyCycleRight = self.diffStep
                else:
                    # Set the maximum duty cycle with difference
                    self.dutyCycleLeft -= self.diffStep/2
                    self.dutyCycleRight += self.diffStep/2
                # Set the duty cycle
                self.changeDutyCycle(self.dutyCycleLeft, self.dutyCycleRight)
            elif strCmd == "B-L-D":
                # Find the minimum duty cycle
                minDutyCycle = min(self.dutyCycleLeft, self.dutyCycleRight)
                # Check if the minimum duty cycle minus the difference required is lower than 0 limit
                if minDutyCycle - self.diffStep - self.linearStep < 0:
                    self.dutyCycleRight = self.diffStep
                    self.dutyCycleLeft = 0
                    # Set the duty cycle
                    self.changeDutyCycle(self.dutyCycleLeft, self.dutyCycleRight)
                else:
                    # Set the minimum duty cycle with difference
                    self.dutyCycleLeft = minDutyCycle - self.diffStep
                    self.dutyCycleRight = minDutyCycle
                    # Decrease the duty cycle
                    self.changeDutyCycle(self.dutyCycleLeft - self.linearStep, self.dutyCycleRight - self.linearStep)
            # Check for backward right acceleration, constant, or deceleration
            elif strCmd == "B-R-A":
                # Find the maximum duty cycle
                maxDutyCycle = max(self.dutyCycleLeft, self.dutyCycleRight)
                # Check if the maximum duty cycle plus the difference required is larger than 100 limit
                if maxDutyCycle + self.diffStep + self.linearStep > 100:
                    self.dutyCycleRight = 100 - self.diffStep
                    self.dutyCycleLeft = 100
                    # Set the duty cycle
                    self.changeDutyCycle(self.dutyCycleLeft, self.dutyCycleRight)
                else:
                    # Set the maximum duty cycle with difference
                    self.dutyCycleLeft = maxDutyCycle + self.diffStep
                    self.dutyCycleRight = maxDutyCycle
                    # Increase the duty cycle
                    self.changeDutyCycle(self.dutyCycleLeft + self.linearStep, self.dutyCycleRight + self.linearStep)
            elif strCmd == "B-R-C":
                # Find the maximum and minimum duty cycle
                maxDutyCycle = max(self.dutyCycleLeft, self.dutyCycleRight)
                minDutyCycle = min(self.dutyCycleLeft, self.dutyCycleRight)
                # Check if the maximum duty cycle plus the difference required is larger than 100 limit
                if maxDutyCycle + self.diffStep/2 > 100:
                    self.dutyCycleRight = 100 - self.diffStep
                    self.dutyCycleLeft = 100
                elif minDutyCycle - self.diffStep/2 < 0:
                    self.dutyCycleLeft = self.diffStep
                    self.dutyCycleRight = 0
                else:
                    # Set the maximum duty cycle with difference
                    self.dutyCycleLeft += self.diffStep/2
                    self.dutyCycleRight -= self.diffStep/2
                # Set the duty cycle
                self.changeDutyCycle(self.dutyCycleLeft, self.dutyCycleRight)
            elif strCmd == "B-R-D":
                # Find the minimum duty cycle
                minDutyCycle = min(self.dutyCycleLeft, self.dutyCycleRight)
                # Check if the minimum duty cycle minus the difference required is lower than 0 limit
                if minDutyCycle - self.diffStep - self.linearStep < 0:
                    self.dutyCycleRight = 0
                    self.dutyCycleLeft = self.diffStep
                    # Set the duty cycle
                    self.changeDutyCycle(self.dutyCycleLeft, self.dutyCycleRight)
                else:
                    # Set the minimum duty cycle with difference
                    self.dutyCycleLeft = minDutyCycle
                    self.dutyCycleRight = minDutyCycle - self.diffStep
                    # Decrease the duty cycle
                    self.changeDutyCycle(self.dutyCycleLeft - self.linearStep, self.dutyCycleRight - self.linearStep)
